package kalahgame.controller;

import kalahgame.model.Board;
import kalahgame.service.KalahGame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

/**
 * Created by ������ on 30.04.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {KalahGame.class, Board.class})
public class KalahGameTest {
    @Autowired
    private KalahGame kalahGame;

    @Before
    public void setUp(){
        kalahGame.getBoard().init(6, 3);
    }

    @Test
    public void testDoSowing() throws Exception {
        Assert.assertEquals(Board.Side.NORTH, kalahGame.doSowing(Board.Side.NORTH, 3));
        Assert.assertEquals(1, kalahGame.getBoard().getSeedsInStore(Board.Side.NORTH));
        Assert.assertEquals(Board.Side.SOUTH, kalahGame.doSowing(Board.Side.NORTH, 0));
        Assert.assertEquals(5, kalahGame.getBoard().getSeedsInStore(Board.Side.NORTH));
    }

    @Test
    public void testGetScore() throws Exception {
        Assert.assertEquals(0, kalahGame.getScore(Board.Side.NORTH));
        kalahGame.doSowing(Board.Side.NORTH, 0);
        kalahGame.doSowing(Board.Side.NORTH, 1);
        kalahGame.doSowing(Board.Side.NORTH, 2);
        kalahGame.doSowing(Board.Side.NORTH, 3);
        kalahGame.doSowing(Board.Side.NORTH, 4);
        kalahGame.doSowing(Board.Side.NORTH, 5);
        Assert.assertEquals(4, kalahGame.getScore(Board.Side.NORTH));
    }

    @Test
    public void testIsFinished() throws Exception {
        kalahGame.doSowing(Board.Side.NORTH, 0);
        kalahGame.doSowing(Board.Side.NORTH, 1);
        kalahGame.doSowing(Board.Side.NORTH, 2);
        kalahGame.doSowing(Board.Side.NORTH, 3);
        kalahGame.doSowing(Board.Side.NORTH, 4);
        kalahGame.doSowing(Board.Side.NORTH, 5);
        Assert.assertTrue(kalahGame.isFinished());
    }
}