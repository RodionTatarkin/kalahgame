package kalahgame.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by ������ on 30.04.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Board.class})
public class BoardTest {
    @Autowired
    private Board board;

    @Before
    public void setUp(){
        board.init(6, 3);
    }

    @Test
    public void testGetHouses() throws Exception {
        Assert.assertEquals(6, board.getHouses());
    }

    @Test
    public void testGetSeedsInHouse() throws Exception {
        for(int i = 0; i < board.getHouses(); i++){
            Assert.assertEquals(3, board.getSeedsInHouse(Board.Side.NORTH, i));
            Assert.assertEquals(3, board.getSeedsInHouse(Board.Side.NORTH, i));
        }
    }

    @Test
    public void testRemoveSeedsFromHouse() throws Exception {
        board.removeSeedsFromHouse(Board.Side.NORTH, 1);
        Assert.assertEquals(0, board.getSeedsInHouse(Board.Side.NORTH, 1));
    }

    @Test
    public void testAddSeedsToHouse() throws Exception {
        board.addSeedsToHouse(Board.Side.NORTH, 1, 10);
        Assert.assertEquals(13, board.getSeedsInHouse(Board.Side.NORTH, 1));
    }

    @Test
    public void testGetSeedsInStore() throws Exception {
        Assert.assertEquals(0, board.getSeedsInStore(Board.Side.NORTH));
    }

    @Test
    public void testGetSeedsInHouseOpposite() throws Exception {
        board.addSeedsToHouse(Board.Side.SOUTH, 4, 10);
        Assert.assertEquals(13, board.getSeedsInHouseOpposite(Board.Side.NORTH, 1));
    }

    @Test
    public void testRemoveSeedsFromHouseOpposite() throws Exception {
        board.removeSeedsFromHouseOpposite(Board.Side.NORTH, 3);
        Assert.assertEquals(0, board.getSeedsInHouse(Board.Side.SOUTH, 2));
    }

    @Test
    public void testAddSeedsToStore() throws Exception {
        board.addSeedsToStore(Board.Side.NORTH, 10);
        Assert.assertEquals(10, board.getSeedsInStore(Board.Side.NORTH));
    }
}