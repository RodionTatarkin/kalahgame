package kalahgame.controller;

import kalahgame.service.KalahGame;
import kalahgame.model.Board;
import kalahgame.service.Player;
import kalahgame.view.GameView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.security.InvalidParameterException;

/**
 * Created by ������ on 29.04.2015.
 */
@Controller
public class KalahGameController {
    @Autowired
    private Player player;
    @Autowired
    private Board board;
    @Autowired
    private KalahGame kalahGame;
    @Autowired
    private GameView gameView;

    private void doUserMove() throws IOException {
        boolean legalMoveTyped = false;
        while(!legalMoveTyped) {
            try {
                int house = gameView.getHouseNumber();

                kalahGame.doSowing(kalahGame.getNextPlayerSide(), house);

                legalMoveTyped = true;
            } catch (IllegalArgumentException e) {
                gameView.printError("You move is incorrect. Try again");
            }
        }
        gameView.printGameState(board);
    }

    private void doPCMove(){
        kalahGame.doSowingWithPlayer(kalahGame.getNextPlayerSide(), player);
        gameView.printGameState(board);
    }

    @PostConstruct
    public void start() throws IOException {
        gameView.printGameStarted();
        gameView.printGameState(board);
        while (!kalahGame.isFinished()){
            if (kalahGame.getNextPlayerSide() == Board.Side.NORTH) {
                doUserMove();
            } else {
                doPCMove();
            }
        }
        gameView.gameFinished(kalahGame.getScore(Board.Side.NORTH), kalahGame.getScore(Board.Side.SOUTH));
    }
}
