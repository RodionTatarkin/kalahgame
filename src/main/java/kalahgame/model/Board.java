package kalahgame.model;


import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Created by ������ on 29.04.2015.
 */
@Component
public class Board {
    public enum Side {
        NORTH(0), SOUTH(1);

        private int rowIndex;

        Side(int rowIndex) {
            this.rowIndex = rowIndex;
        }

        public Side opposite(){
            switch (this){
                case NORTH:
                    return SOUTH;
                case SOUTH:
                    return NORTH;
                default:
                    return NORTH;
            }
        }

        public int getRowIndex() {
            return rowIndex;
        }
    }

    private static final int HOUSES_NUMBER = 6;
    private static final int SEEDS_NUMBER = 3;
    private int houses;
    private int[][] board;

    public void init(int houses, int seeds){
        this.houses = houses;
        board = new int[2][houses + 1];
        for (int i = 0; i < houses; i++){
            board[Side.NORTH.getRowIndex()][i] = seeds;
            board[Side.SOUTH.getRowIndex()][i] = seeds;
        }
        board[Side.NORTH.getRowIndex()][houses] = 0;
        board[Side.SOUTH.getRowIndex()][houses] = 0;
    }

    public Board() {
        init(HOUSES_NUMBER, SEEDS_NUMBER);
    }

    public Board(int houses, int seeds) {
        if (houses < 1){
            throw new IllegalArgumentException("There has to be at least one house");
        }
        if (seeds < 0){
            throw new IllegalArgumentException("There has to be a non-negative number of seeds per house");
        }
        init(houses, seeds);
    }

    public int getHouses() {
        return houses;
    }

    public int getSeedsInHouse(Side side, int house){
        checkValidHouseIndex(house);
        return board[side.getRowIndex()][house];
    }

    public void removeSeedsFromHouse(Side side, int house){
        checkValidHouseIndex(house);
        board[side.getRowIndex()][house] = 0;
    }

    public void addSeedsToHouse(Side side, int house, int seeds){
        checkValidHouseIndex(house);
        board[side.getRowIndex()][house] += seeds;
    }

    public int getSeedsInStore(Side side){
        return board[side.getRowIndex()][houses];
    }

    public int getSeedsInHouseOpposite(Side side, int house){
        checkValidHouseIndex(house);
        return board[side.opposite().getRowIndex()][houses - 1 - house];
    }

    public void removeSeedsFromHouseOpposite(Side side, int house){
        checkValidHouseIndex(house);
        board[side.opposite().getRowIndex()][houses - 1 - house] = 0;
    }

    public void addSeedsToStore(Side side, int seeds){
        board[side.getRowIndex()][houses] += seeds;
    }

    public boolean hasAllHousesEmpty(Side side){
        for (int houseIndex = 0; houseIndex < houses; houseIndex++){
            if (board[side.getRowIndex()][houseIndex] != 0){
                return false;
            }
        }
        return true;
    }

    private void checkValidHouseIndex(int index){
        if (index < 0 || index >= houses){
            throw new IllegalArgumentException("Invalid index " + index + " of house");
        }
    }
}
