package kalahgame.service;

import kalahgame.model.Board;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;

/**
 * Created by ������ on 29.04.2015.
 */
@Service
public class KalahGame {
    @Autowired
    private Board board;

    private Board.Side nextPlayerSide = Board.Side.NORTH;

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    private boolean isLegalMove(Board.Side side, int house){
        return (house >= 0) && (house < board.getHouses()) && (board.getSeedsInHouse(side, house) > 0);
    }

    public Board.Side doSowing(Board.Side side, int house) throws IllegalArgumentException{
        if (!isLegalMove(side, house)){
            throw new IllegalArgumentException("Invalid move: " + side + ", " + house);
        }
        int seedsToSow = board.getSeedsInHouse(side, house);
        board.removeSeedsFromHouse(side, house);
        int numOfHouses = board.getHouses();
        int numOfPitsToReceiveSeedsInCircle = 2 * numOfHouses + 1;
        int circles = seedsToSow / numOfPitsToReceiveSeedsInCircle;
        int extraSeeds = seedsToSow % numOfPitsToReceiveSeedsInCircle;

        if (circles != 0) {
            for (int houseIndex = 0; houseIndex < numOfHouses; houseIndex++) {
                board.addSeedsToHouse(Board.Side.NORTH, houseIndex, circles);
                board.addSeedsToHouse(Board.Side.SOUTH, houseIndex, circles);
            }
            board.addSeedsToStore(side, circles);
        }
        int currentPitToSow = house + 1;
        Board.Side currentSide = side;
        while (extraSeeds > 0){
            if (currentPitToSow == numOfHouses){
                if (currentSide == side){
                    board.addSeedsToStore(side, 1);
                    currentPitToSow = 0;
                } else {
                    board.addSeedsToHouse(side.opposite(), 0, 1);
                    currentPitToSow = 1;
                }
                currentSide = side.opposite();
            } else {
                board.addSeedsToHouse(currentSide, currentPitToSow, 1);
                currentPitToSow++;
            }
            extraSeeds--;
        }
        if ((currentSide == side) && (board.getSeedsInHouse(side, currentPitToSow - 1) == 1) && (board.getSeedsInHouseOpposite(side, currentPitToSow - 1) > 0)){
            board.removeSeedsFromHouse(side, currentPitToSow - 1);
            board.addSeedsToStore(side, 1 + board.getSeedsInHouseOpposite(side, currentPitToSow - 1));
            board.removeSeedsFromHouseOpposite(side, currentPitToSow - 1);
        }
        if (currentPitToSow == 0){
            nextPlayerSide = side;
        } else {
            nextPlayerSide = side.opposite();
        }
        return nextPlayerSide;
    }

    public Board.Side doSowingWithPlayer(Board.Side side, Player player){
        return doSowing(side, player.getHouse(side, board));
    }

    public boolean isFinished(){
        return board.hasAllHousesEmpty(Board.Side.NORTH) || board.hasAllHousesEmpty(Board.Side.SOUTH);
    }

    public Board.Side getNextPlayerSide() {
        return nextPlayerSide;
    }

    public int getScore(Board.Side side){
        if (isFinished()){
            int score = board.getSeedsInStore(side);
            for (int houseIndex = 0; houseIndex < board.getHouses(); houseIndex++){
                score += board.getSeedsInHouse(side, houseIndex);
            }
            return score;
        }
        return board.getSeedsInStore(side);
    }
}
