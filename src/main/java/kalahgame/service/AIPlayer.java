package kalahgame.service;

import kalahgame.service.Player;
import kalahgame.model.Board;
import org.springframework.stereotype.Component;

/**
 * Created by ������ on 30.04.2015.
 */
@Component
public class AIPlayer implements Player {
    public int getHouse(Board.Side side, Board board) {
        for (int houseIndex = 0; houseIndex < board.getHouses(); houseIndex++){
            if (board.getSeedsInHouse(side, houseIndex) > 0){
                return houseIndex;
            }
        }
        return 0;
    }
}
