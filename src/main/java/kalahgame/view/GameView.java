package kalahgame.view;

import kalahgame.model.Board;

import java.io.IOException;

/**
 * Created by ������ on 30.04.2015.
 */
public interface GameView {
    void printGameStarted();
    void printGameState(Board board);
    void gameFinished(int userScore, int pcScore);
    int getHouseNumber() throws IOException;
    void printError(String errorString);
}
