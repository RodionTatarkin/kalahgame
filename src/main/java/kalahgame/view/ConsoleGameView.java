package kalahgame.view;

import kalahgame.model.Board;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ������ on 30.04.2015.
 */
@Component
public class ConsoleGameView implements GameView {

    @Override
    public void printGameStarted() {
        System.out.println("The Kalah Game starts!");
    }

    @Override
    public void printGameState(Board board) {
        System.out.println("===============");
        System.out.println("Game state:");
        System.out.print("User: ");
        for (int houseIndex = board.getHouses() - 1; houseIndex >= 0; houseIndex--){
            System.out.print(board.getSeedsInHouse(Board.Side.NORTH, houseIndex));
            System.out.print(" ");
        }
        System.out.println();
        System.out.print("     ");
        System.out.print(board.getSeedsInStore(Board.Side.NORTH));
        for (int i = 0; i < board.getHouses(); i++){
            System.out.print("  ");
        }
        System.out.print(board.getSeedsInStore(Board.Side.SOUTH));
        System.out.println();
        System.out.print("PC:   ");
        for (int houseIndex = 0; houseIndex < board.getHouses(); houseIndex++){
            System.out.print(board.getSeedsInHouse(Board.Side.SOUTH, houseIndex));
            System.out.print(" ");
        }
        System.out.println();
    }

    @Override
    public void gameFinished(int userScore, int pcScore) {
        System.out.println("Game Finished");
        System.out.println("User score: " + userScore);
        System.out.println("PC score: " + pcScore);
    }

    @Override
    public int getHouseNumber() throws IOException {
        System.out.println("Enter pit number to move:");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        boolean numberTyped = false;
        int house = 0;
        while (!numberTyped) {
            String line = in.readLine();
            if (line == null) {
                throw new IOException();
            }
            try {
                house = Integer.parseInt(line);
            } catch (NumberFormatException e) {
                System.out.println("It's not a number. Try again");
                continue;
            }
            numberTyped = true;
        }
        return house - 1;
    }

    @Override
    public void printError(String errorString) {
        System.out.println(errorString);
    }
}
